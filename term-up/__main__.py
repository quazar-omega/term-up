import argparse
import shutil
import subprocess
import time

from stringcolor import cs

def center_colored_string(colored_string: str, original_string: str, filler: str = " ") -> str:
	columns = shutil.get_terminal_size().columns
	columns -= len(original_string)

	centered_line: str = ""
	for i in range(columns):
		if i == columns // 2 :
			centered_line += colored_string
		else:
			centered_line += filler
	
	return centered_line

def run(filename: str, arguments: list[str], icons: bool):
	title = "\""+ str(filename) + "\" running"

	
	top_left_side = cs(" ", "white", "blue")
	top_right_side = cs(" ", "white", "blue")
	if icons:
		top_left_side = cs("", "blue")
		top_right_side = cs("", "blue")
	
	print(
		center_colored_string(
			top_left_side +
			cs(title, "white", "blue") +
			top_right_side,
			" " + title + " "
		)
	)

	time_start = time.time()
	process = subprocess.Popen([filename] + arguments, shell=True);
	return_value = process.wait()
	time_elapsed = time.time() - time_start

	return_stat = "Process returned: " + str(return_value) + " "
	time_stat =  " Elapsed: " + str(time_elapsed) + "s"

	return_stat_fg = "white"
	if return_value != 0:
		return_stat_bg = "red"
	else :
		return_stat_bg = "green"
	
	time_stat_fg = "black"
	time_stat_bg = "yellow"

	
	left_side = cs(" ", return_stat_fg, return_stat_bg)
	right_side = cs(" ", time_stat_fg, time_stat_bg)

	if icons:
		if return_value:
			return_stat = "✘ " + return_stat
		else:
			return_stat = "✔ " + return_stat
		
		time_stat = " " + time_stat
		left_side = cs("", return_stat_bg)
		right_side = cs("", time_stat_bg)

	print("\n" + center_colored_string(
			left_side +
			cs(return_stat, return_stat_fg, return_stat_bg) + 
			cs(time_stat, time_stat_fg, time_stat_bg) +
			right_side,
			" " + return_stat + time_stat + " "
		)
	)

	print("\nPress " + cs(" ENTER ↲ ", "black", "white") + " to exit: ")
	input()

def spawn_terminal(
		terminal: str, spawn_arguments: list[str],
		term_up_arguments: list[str], 
		filename: str, arguments: list[str]
	):
	command: list[str] = [terminal] + spawn_arguments + \
		["python", "-m", "term-up"] + term_up_arguments + ["--run"] + \
		[filename] + arguments
	
	print(" ".join(str(elem) for elem in (command)))
	subprocess.Popen(command)



if __name__ == "__main__":
	parser: argparse.ArgumentParser = argparse.ArgumentParser(prog="term-up", description="Execute terminal apps in a new window")



	parser.add_argument(
		"-i",
		"--icons",
		help="Enable icons and other nerd font characters",
		action="store_true"
	)

	parser.add_argument(
		"-r",
		"--run",
		help="Run the app inside the terminal window",
		type=str,
		nargs=argparse.REMAINDER
	)

	parser.add_argument(
		"file",
		help="The file to run along with its arguments",
		type=str,
		nargs=argparse.REMAINDER
	)

	args = parser.parse_args()

	if args.file:
		forward_arguments: list[str] = []
		if args.icons:
			forward_arguments += ["--icons"]
		spawn_terminal("kitty", ["-T",  args.file[0]], forward_arguments, args.file[0], args.file[1:])
	if args.run:
		run(args.run[0], args.run[1:], args.icons)