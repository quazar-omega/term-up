#!/usr/bin/env python

from distutils.core import setup

setup(
	name="term-up",
	version="0.1",
	description="Execute terminal apps in a new window",
	author="quazar-omega",
	packages=["term-up"]
)